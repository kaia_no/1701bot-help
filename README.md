MemoryAlpha is a bot on /1701/tube. All commands work by private message to MemoryAlpha as well.

| Command      | Description                                                                     | Example                                                                        |
|--------------|---------------------------------------------------------------------------------|--------------------------------------------------------------------------------|
| !roll  <num> | Roll random integer. With no <num> provided from 1 to 10. <num> up to 10 digits | !roll, !roll 20                                                                |
| !ms    <str> | Memory Alpha search articles. Retrieves as many as fit in one chat message      | !ms bajoran scum                                                               |
| !ma    <str> | Memory Alpha get beginning of article                                           | !ma section 31                                                                 |
| !bs    <str> | Memory Beta search articles. Retrieves as many as fit in one chat message       | !bs bajoran scum                                                               |
| !ba    <str> | Memory Beta get beginning of article                                            | !ba section 31                                                                 |
| !ws    <str> | Wikipedia search articles                                                       | !ws white russian                                                              |
| !wa    <str> | Wikipedia beginning of article                                                  | !wa potato                                                                     |
| !current     | Current media played                                                            | !current                                                                       |
| !next        | Next media queued                                                               | !next                                                                          |
| !seen  <str> | Last time user sent message to chat (database has to build first - takes time!) | !seen lecake                                                                   |
| !first <str> | The first time the bot saw the user - database since May 2019                   | !first muscles                                                                 |
| !ra    <num> | Rules of Acquisition. If no <num> provided, random rule.                        | !ra 1                                                                          |
| !om    <num> | OMDb API. Returns info on movies like actors and rating.                        | !om videodrome                                                                 |


For help, reporting bugs and feature requests please ask kaia in chat or on Discord (kaia#6033).

Open issues:

 * !wa sometimes breaks via lxml; not the bot's fault but the `wikipedia` module

Features in the pipeline:
 * !shutdown  -  emergency shutdown
 * !next      -  show more episodes than 2 if requested
 * !summary   -  episode summary
 * !mentions  -  last mentions of your username in chat